import React, { Fragment } from 'react';

const ImageBuild = () => {
    return (
        <Fragment>
            <img
                src='https://adtadmin.ferociousmediaweb.com/uploads/big-placeholder.jpg'
                alt='test image'
            />
        </Fragment>
    );
};

export default ImageBuild;
