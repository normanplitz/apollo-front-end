import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid } from '@mui/material';
import Pagination from 'reactjs-hooks-pagination';
import { Link } from 'react-router-dom';
import Spinning from '../Extras/Spinning';
import { useNavigate  } from "react-router-dom";
import { TextField, Autocomplete, Alert} from '@mui/material';

// CSS
import './products.css';
import ProductItem from './ProductItem';
import { addToInquiry, removeFromInq } from '../../actions/inquiryActions';
import axios from "axios";

document.body.classList.add('search-body');

const SearchedProducts = (props) => {

    //let history = useHistory()
    const dispatch = useDispatch();
    const [career_list, set_career_list] = useState([])

    const [currentPage, setCurrentPage] = useState([])

    const [pathname, setPathname] = useState([]);

    const [loading, setLoading] = useState(true);

    const [pageLimit, setPageLimit] = useState(
        localStorage.getItem('perpage') ? localStorage.getItem('perpage') : 10
    );

	let optionsSort = useMemo(() => [], []);

	const [filterSort, setFilterSort] = useState(
        localStorage.getItem('sorting')
            ? localStorage.getItem('sorting')
            : 'interface'
    );

	const [isDisabled, setIsDisabled] = useState(true);
    const [isDisabled2, setIsDisabled2] = useState(false);

	const ascendingOrder = () => {
        setIsDisabled(true);
        setIsDisabled2(false);
        setSort('asc');
        var list = makeAscending(productsList);
        setProductsList(list);
        setCurrentPage(
            productsList && productsList.slice(offset, offset + parseInt(pageLimit, 10))
        );
    };

    const descendingOrder = () => {
        setIsDisabled(false);
        setIsDisabled2(true);
        setSort('des');
        var list = makeDescending(productsList);
        setProductsList(list);
        setCurrentPage(
            productsList && productsList.slice(offset, offset + parseInt(pageLimit, 10))
        );
    };

	const [sortList, setSort] = useState(
        localStorage.getItem('alphabetical')
            ? localStorage.getItem('alphabetical')
            : 'asc'
    );

	const onChangeSelect = (e) => {
        e.preventDefault();
        localStorage.setItem('sorting', `${e.target.value}`);
        setFilterSort(e.target.value);
    };

	const onChange = (e) => {
        e.preventDefault();
        if (e.target.value === '10') {
            setPageLimit(10);
        } else if (e.target.value === '20') {
            setPageLimit(20);
        } else if (e.target.value === '50') {
            setPageLimit(50);
        } else if (e.target.value === '75') {
            setPageLimit(75);
        } else if (e.target.value === '100') {
            setPageLimit(100);
        }
    };



	let query = new URLSearchParams(window.location.search)

    const onPageChanged = (page) => {
        const offset = (page - 1) * pageLimit;
        setOffset(offset);

        setCurrentPage(
            productsList && productsList.slice(offset, offset + parseInt(pageLimit, 10))
        );
    };

    function makeAscending(array) {
        return array.reverse();
    }

    function makeDescending(array) {
        return array.reverse();
    }



	const [productsList, setProductsList] = useState([]);
	const [offset, setOffset] = useState(0);
    useEffect(() => {
		axios.get('https://adtadmin.ferociousmediaweb.com/api/products/searchProduct?search=' + query.get('search')).then(resp => {

            function compare_lname( a, b )
            {
                if ( a.title.toLowerCase() < b.title.toLowerCase()){
                    return -1;
                }
                if ( a.title.toLowerCase() > b.title.toLowerCase()){
                    return 1;
                }
                return 0;
            }

            resp.data.sort(compare_lname);
            setProductsList(resp.data);
            setLoading(false);
        });

	}, []);

    useEffect(() => {
        setCurrentPage(
            productsList && productsList.slice(offset, offset + parseInt(pageLimit, 10))
        );
    }, [productsList])


    const [productsList2, setProductsList2] = useState([]);
    const [selectedProduct2, setSelectedProduct2] = useState('');
    const [inputValue2, setInputValue2] = useState('');

    const history2 = useNavigate();


    useEffect(() => {
      axios.get('https://adtadmin.ferociousmediaweb.com/api/products/searchProduct?search=' + inputValue2).then(resp => {
          setProductsList2(resp.data);
      });

    }, [inputValue2]);



	return loading ? (
        <Spinning />
    ) : productsList.length === 0 ? (
        <section className='no-product'>
        <Grid container justifyContent='center' spacing={5}>

            <Grid item xs={12} sm={12} md={8} lg={9}>
          <h2>Search Results for "{query.get('search')}"</h2>
          <p>No products were found.</p>

          <div className="free-solo-search-box">
          <i className="far fa-search"></i>
          <Autocomplete
            freeSolo
            id="free-solo-3-demo"
            disableClearable
            options={productsList2}
            getOptionLabel={option => option.fmtitle != undefined?option.fmtitle:''}
            value={selectedProduct2}
            onChange={(event, newValue) => {

              if (!newValue.fmtitle){
                //setSelectedProduct(newValue);
                setInputValue2('');
                history2("/products/search/?search="+newValue);
              } else {
                //setSelectedProduct(newValue);
                setInputValue2('');
                history2("/products/search/?search="+newValue.fmtitle);
              }

              if(window.location.href.includes("products/search")){
                window.location.reload(false);
              }
            }}
            inputValue={inputValue2}
            onInputChange={(event, newInputValue) => {
              setInputValue2(newInputValue);
            }}
            renderInput={(params) => (
            <TextField
              {...params}
              label={query.get('search')}
              InputProps={{
              ...params.InputProps,
              type: 'search',
              }}
            />
            )}
          />
          </div>

          </Grid>
      </Grid>
        </section>
    ) : (
        <div id='filter-boundary' className='products-block'>
            <section>
                <Grid container justifyContent='center' spacing={5}>

                    <Grid item xs={12} sm={12} md={8} lg={9}>
                        <br/><br/><br/><br/><br/><br/><br/><br/>
                        <h2>Search Results for "{query.get('search')}"</h2>

                        <div className="free-solo-search-box">
                        <i className="far fa-search"></i>
                        <Autocomplete
                          freeSolo
                          id="free-solo-3-demo"
                          disableClearable
                          options={productsList2}
                          getOptionLabel={option => option.fmtitle != undefined?option.fmtitle:''}
                          value={selectedProduct2}
                          onChange={(event, newValue) => {

                            if (!newValue.fmtitle){
                              //setSelectedProduct(newValue);
                              setInputValue2('');
                              history2("/products/search/?search="+newValue);
                            } else {
                              //setSelectedProduct(newValue);
                              setInputValue2('');
                              history2("/products/search/?search="+newValue.fmtitle);
                            }

                            if(window.location.href.includes("products/search")){
                              window.location.reload(false);
                            }
                          }}
                          inputValue={inputValue2}
                          onInputChange={(event, newInputValue) => {
                            setInputValue2(newInputValue);
                          }}
                          renderInput={(params) => (
                          <TextField
                            {...params}
                            label={query.get('search')}
                            InputProps={{
                            ...params.InputProps,
                            type: 'search',
                            }}
                          />
                          )}
                        />
                        </div>

                        <Grid container style={{ marginBottom: 70 }}>
                            <Grid
                                item
                                xs={12}
                                className='top-sorting-container'
                            >
                                <div className='top-sorting'>
                                    <span>Items Per Page:</span>
                                    <select
                                        className='sorting-dropdown'
                                        name='perpage'
                                        value={pageLimit}
                                        onChange={(e) => onChange(e)}
                                    >
                                        <option
                                            className='sorting-dropdown-option'
                                            value='10'
                                        >
                                            10
                                        </option>
                                        <option
                                            className='sorting-dropdown-option'
                                            value='20'
                                        >
                                            20
                                        </option>
                                        <option
                                            className='sorting-dropdown-option'
                                            value='50'
                                        >
                                            50
                                        </option>
                                        <option
                                            className='sorting-dropdown-option'
                                            value='75'
                                        >
                                            75
                                        </option>
                                        <option
                                            className='sorting-dropdown-option'
                                            value='100'
                                        >
                                            100
                                        </option>
                                    </select>
                                </div>
                                {/* <div className='filter-dropdown top-sorting'>
                                    <span>Sort By:</span>
                                    {optionsSort.length > 0 && (
                                        <select
                                            id="sortingDropdown"
                                            className='sorting-dropdown'
                                            name='filtersort'
                                            onChange={(e) => onChangeSelect(e)}
                                            defaultValue={filterSort}
                                            style={{
                                                textTransform: 'none',
                                                fontFamily: 'var(--fontuno)',
                                            }}
                                        >
                                            {optionsSort.map((op, index) => (
                                                <option
                                                    key={index}
                                                    className='sorting-dropdown-option'
                                                    value={op.type}
                                                >
                                                    {op.label}
                                                </option>
                                            ))}
                                        </select>
                                    )}
                                </div> */}
                                <div className='top-sorting'>
                                    <span>Order:</span>
                                    <div className='aphabetical'>
                                        <button
                                            onClick={ascendingOrder}
                                            className='apollo-button filter-buttons'
                                            disabled={isDisabled}
                                        >
                                            Ascending
                                        </button>{' '}
                                        <button
                                            onClick={descendingOrder}
                                            className='apollo-button filter-buttons'
                                            disabled={isDisabled2}
                                        >
                                            Descending
                                        </button>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>

                        {currentPage.length > 0 && (
                            <div className='list-container'>
                                {currentPage.length > 0 &&
                                    currentPage.map((product) => (
                                        <ProductItem
                                            pathname={pathname}
                                            product={product}
                                            key={product._id}
                                        />
                                    ))}
                            </div>
                        )}

                        <Grid container justifyContent='center'>
                            <Grid item xs={12} sm={5} md={4}>
                                <div className='pagination-wrap'>
                                    {productsList && (
                                        <Pagination
                                            totalRecords={productsList.length}
                                            pageLimit={pageLimit}
                                            pageRangeDisplayed={3}
                                            onChangePage={onPageChanged}
                                        />
                                    )}
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </section>
        </div>
    );
};
export default SearchedProducts;
