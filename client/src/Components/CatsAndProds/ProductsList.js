import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid } from '@mui/material';
import Pagination from 'reactjs-hooks-pagination';

// CSS
import './products.css';

// Actions
import { filteredProducts, listProducts } from '../../actions/productActions';

// Parts
import Spinning from '../Extras/Spinning';
import ProductItem from './ProductItem';
import FiltersSidebar from '../Extras/FiltersSidebar';

const ProductsList = ({ pathname, title, title2, forApi }) => {
    const dispatch = useDispatch();

    const productFilteredList = useSelector(
        (state) => state.productFilteredList
    );
    const { error, filtered } = productFilteredList;
    const productList = useSelector((state) => state.productList);
    const { products } = productList;

    let optionsSort = useMemo(() => [], []);

    const [productsBycat, setProductsBycat] = useState([]);
    const [filterCombination, setFiltercombination] = useState([]);
    const [noFilterResults, setNoFilterResults] = useState(true);
    const [finalProducts, setFinalProducts] = useState([]);
    const [initialFilters, setInitialFilters] = useState([]);
    const [isNormal, setIsNormal] = useState(true);
    const [getFilters, setGetFilters] = useState({
        serie: '',
        category: '',
        forManu: [],
        forDiag: [],
        forRes: [],
        forBright: [],
        forInter: [],
        forAngle: [],
        forTemp: [],
        forTouch: [],
        forFormat: [],
        forPower: [],
        forSize: [],
        forCont: [],
        forInput: [],
        forOutput: [],
        forCpu: [],
        forCpuModel: [],
        forFunction: [],
        forOpt: [],
    });
    const [loading, setLoading] = useState(true);
    const [sorted, setSorted] = useState([]);
    const [sortList, setSort] = useState(
        localStorage.getItem('alphabetical')
            ? localStorage.getItem('alphabetical')
            : 'asc'
    );
    const [isDisabled, setIsDisabled] = useState(false);
    const [isDisabled2, setIsDisabled2] = useState(false);
    const [filterSort, setFilterSort] = useState(
        localStorage.getItem('sorting')
            ? localStorage.getItem('sorting')
            : 'interface'
    );
    const [pageLimit, setPageLimit] = useState(
        localStorage.getItem('perpage') ? localStorage.getItem('perpage') : 10
    );
    const [offset, setOffset] = useState(0);
    const [currentPage, setCurrentPage] = useState([]);
    const [perpage, setPerpage] = useState(0);
    const [forManu, setForManu] = useState(
        localStorage.getItem('manufacturers')
            ? JSON.parse(localStorage.getItem('manufacturers'))
            : []
    );
    const [forDiag, setForDiag] = useState(
        localStorage.getItem('diagonal')
            ? JSON.parse(localStorage.getItem('diagonal'))
            : []
    );
    const [forRes, setForRes] = useState(
        localStorage.getItem('resolutions')
            ? JSON.parse(localStorage.getItem('resolutions'))
            : []
    );
    const [forBright, setForBright] = useState(
        localStorage.getItem('brightness')
            ? JSON.parse(localStorage.getItem('brightness'))
            : []
    );
    const [forInter, setForInter] = useState(
        localStorage.getItem('interface')
            ? JSON.parse(localStorage.getItem('interface'))
            : []
    );
    const [forAngle, setForAngle] = useState(
        localStorage.getItem('angle')
            ? JSON.parse(localStorage.getItem('angle'))
            : []
    );
    const [forTemp, setForTemp] = useState(
        localStorage.getItem('temperature')
            ? JSON.parse(localStorage.getItem('temperature'))
            : []
    );
    const [forCpu, setForCpu] = useState(
        localStorage.getItem('cpus')
            ? JSON.parse(localStorage.getItem('cpus'))
            : []
    );
    const [forCpuModel, setForCpuModel] = useState(
        localStorage.getItem('cpumodels')
            ? JSON.parse(localStorage.getItem('cpumodels'))
            : []
    );
    const [forFunction, setForFunction] = useState(
        localStorage.getItem('functions')
            ? JSON.parse(localStorage.getItem('functions'))
            : []
    );

    const [forTouch, setForTouch] = useState(
        localStorage.getItem('touchscreen')
            ? localStorage.getItem('touchscreen')
            : []
    );
    const [forFormat, setForFormat] = useState(
        localStorage.getItem('format')
            ? localStorage.getItem('format')
            : []
    );
    const [forPower, setForPower] = useState(
        localStorage.getItem('power')
            ? localStorage.getItem('power')
            : []
    );
    const [forSize, setForSize] = useState(
        localStorage.getItem('size')
            ? localStorage.getItem('size')
            : []
    );
    const [forCont, setForCont] = useState(
        localStorage.getItem('cont')
            ? localStorage.getItem('cont')
            : []
    );
    const [forOpt, setForOpt] = useState(
        localStorage.getItem('option')
            ? localStorage.getItem('option')
            : []
    );
    const [forInput, setForInput] = useState(
        localStorage.getItem('inputs')
            ? localStorage.getItem('inputs')
            : []
    );
    const [forOutput, setForOutput] = useState(
        localStorage.getItem('outputs')
            ? localStorage.getItem('outputs')
            : []
    );
    useEffect(() => {
        let resetFb = document.getElementById('filters-container');

        if (resetFb !== null) {
            resetFb.style.transform = 'translateY(' + 0 + 'px)';
        }

        // eslint-disable-next-line
    }, [currentPage]);

    useEffect(() => {
        clearFilters();
        if (forApi.title === 'EOL Products') {
          dispatch(listProducts(forApi.id3));
        }
        if (forApi.title !== '' || forApi.title2 !== '' || forApi.title !== 'EOL Products') {

            dispatch(listProducts(forApi));
        }
    }, [dispatch, forApi, pathname]);

    useEffect(() => {
        const ProductSetup = () => {
            if (!productList.loading && !productList.error && products) {

                const endPr = products && products.length + 1;
                const minusZeroPro = products && products.slice(1, endPr);
                setFinalProducts(minusZeroPro);
                setInitialFilters(products[0]);
            }
        };
        ProductSetup();
        return () => {
            setLoading(true);
            setFinalProducts([]);
        };
    }, [products, productList.error, productList.loading, pathname]);

    useEffect(() => {
        setFiltercombination([]);
        const FilterProducts = () => {
            const end = filtered && filtered.length + 1;
            const minusZero = filtered && filtered.slice(0, end);
            if (filtered && filtered.length > 0) {
                setFiltercombination(minusZero);
                setIsNormal(false);
                setNoFilterResults(false);
            } else {
                setNoFilterResults(true);
            }

            if (finalProducts && finalProducts.length > 0) {
                setProductsBycat(finalProducts);
            }
        };

        FilterProducts();

        return () => {
            setFiltercombination([]);
            setProductsBycat([]);
            setLoading(true);
        };
    }, [finalProducts, filtered, pathname]);

    useEffect(() => {
        if (
            forManu.length > 0 ||
            forDiag.length > 0 ||
            forRes.length > 0 ||
            forBright.length > 0 ||
            forInter.length > 0 ||
            forAngle.length > 0 ||
            forTemp.length > 0 ||
            forTouch.length > 0 ||
            forFormat.length > 0 ||
            forPower.length > 0 ||
            forSize.length > 0 ||
            forInput.length > 0 ||
            forOutput.length > 0 ||
            forCont.length > 0 ||
            forCpu.length > 0 ||
            forCpuModel.length > 0 ||
            forFunction.length > 0 ||
            forOpt.length > 0
        ) {
            setGetFilters({
                serie: title ? title : '',
                category: title2 ? title2 : '',
                id1: forApi.id1 ? forApi.id1 : 0,
                id2: forApi.id2 ? forApi.id2 : 0,
                id3: forApi.id3 ? forApi.id3 : 0,
                forManu: forManu.length > 0 ? forManu : [],
                forDiag: forDiag.length > 0 ? forDiag : [],
                forRes: forRes.length > 0 ? forRes : [],
                forBright: forBright.length > 0 ? forBright : [],
                forInter: forInter.length > 0 ? forInter : [],
                forAngle: forAngle.length > 0 ? forAngle : [],
                forTemp: forTemp.length > 0 ? forTemp : [],
                forCpu: forCpu.length > 0 ? forCpu : [],
                forCpuModel: forCpuModel.length > 0 ? forCpuModel : [],
                forFunction: forFunction.length > 0 ? forFunction : [],
                forTouch: forTouch.length > 0 ? forTouch : [],
                forFormat: forFormat.length > 0 ? forFormat : [],
                forPower: forPower.length > 0 ? forPower : [],
                forSize: forSize.length > 0 ? forSize : [],
                forCont: forCont.length > 0 ? forCont : [],
                forInput: forInput.length > 0 ? forInput : [],
                forOutput: forOutput.length > 0 ? forOutput : [],
                forOpt: forOpt.length > 0 ? forOpt : [],
            });

            if (!noFilterResults) {
                setOffset(0);
                setCurrentPage(
                    sorted && sorted.slice(0, parseInt(pageLimit, 10))
                );
            }

            setIsNormal(false);
        } else {
            setIsNormal(true);
        }

        // eslint-disable-next-line
    }, [
        title,
        title2,
        forManu,
        forDiag,
        forRes,
        forBright,
        forInter,
        forAngle,
        forTemp,
        forTouch,
        forFormat,
        forPower,
        forSize,
        forCont,
        forInput,
        forOutput,
        forCpu,
        forCpuModel,
        forFunction,
        forOpt,
    ]);

    useEffect(() => {
        if (
            getFilters.forManu.length > 0 ||
            getFilters.forDiag.length > 0 ||
            getFilters.forRes.length > 0 ||
            getFilters.forBright.length > 0 ||
            getFilters.forInter.length > 0 ||
            getFilters.forAngle.length > 0 ||
            getFilters.forTemp.length > 0 ||
            getFilters.forTouch.length > 0 ||
            getFilters.forFormat.length > 0 ||
            getFilters.forPower.length > 0 ||
            getFilters.forSize.length > 0 ||
            getFilters.forCont.length > 0 ||
            getFilters.forInput.length > 0 ||
            getFilters.forOutput.length > 0 ||
            getFilters.forCpu.length > 0 ||
            getFilters.forCpuModel.length > 0 ||
            getFilters.forFunction.length > 0 ||
            getFilters.forOpt.length > 0
        ) {
            dispatch(filteredProducts(getFilters));
        } else if (forApi.id3 === 99999){
          dispatch(
              filteredProducts({
                  serie: title ? title : '',
                  category: title2 ? title2 : '',
                  id1: 0,
                  id2: 0,
                  id3: 99999,
                  forManu: [],
                  forDiag: [],
                  forRes: [],
                  forBright: [],
                  forInter: [],
                  forAngle: [],
                  forTemp: [],
                  forTouch: [],
                  forFormat: [],
                  forPower: [],
                  forSize: [],
                  forCont: [],
                  forInput: [],
                  forOutput: [],
                  forCpu: [],
                  forCpuModel: [],
                  forFunction: [],
                  forOpt: [],
              })
          );

        } else {
            dispatch(
                filteredProducts({
                    serie: title ? title : '',
                    category: title2 ? title2 : '',
                    id1: 0,
                    id2: 0,
                    id3: 0,
                    forManu: [],
                    forDiag: [],
                    forRes: [],
                    forBright: [],
                    forInter: [],
                    forAngle: [],
                    forTemp: [],
                    forTouch: [],
                    forFormat: [],
                    forPower: [],
                    forSize: [],
                    forCont: [],
                    forInput: [],
                    forOutput: [],
                    forCpuModel: [],
                    forCpu: [],
                    forFunction: [],
                    forOpt: [],
                })
            );
        }

        // eslint-disable-next-line
    }, [dispatch, getFilters]);

    useEffect(() => {
        const SetData = () => {
            if (filterCombination.length >= 1 && !isNormal) {
                const sorted = [...filterCombination]
                sorted.sort((a, b) => {
                    const isReversed = sortList === 'asc' ? 1 : -1;
                    if (filterSort && a[filterSort] && b[filterSort]) {
                        if (typeof a[filterSort] === 'object' && !Array.isArray(a[filterSort])) {
                            return isReversed * a[filterSort].title.localeCompare(b[filterSort].title);
                        } else if (typeof a[filterSort] !== 'object') {
                            if (typeof a[filterSort] === 'number') {
                                return isReversed * (a[filterSort] - b[filterSort]);
                            }
                            return isReversed * a[filterSort].localeCompare(b[filterSort]);
                        }
                    }

                    return isReversed * a.fmtitle.localeCompare(b.fmtitle);
                });
                setSorted(sorted);
            } else if (filtered && filtered.length === 1) {
                setNoFilterResults(true);
                setSorted([]);
            } else if (productsBycat && productsBycat.length > 0 && isNormal) {
                const sorted = [...productsBycat]
                sorted.sort((a, b) => {
                    const isReversed = sortList === 'asc' ? 1 : -1;
                    if (filterSort && a[filterSort] && b[filterSort]) {
                        if (typeof a[filterSort] === 'object' && !Array.isArray(a[filterSort])) {
                            return isReversed * a[filterSort].title.localeCompare(b[filterSort].title);
                        } else if (typeof a[filterSort] !== 'object') {
                            if (typeof a[filterSort] === 'number') {
                                return isReversed * (a[filterSort] - b[filterSort]);
                            }
                            return isReversed * a[filterSort].localeCompare(b[filterSort]);
                        }
                    }

                    return isReversed * a.fmtitle.localeCompare(b.fmtitle);
                });

                setSorted(sorted);
            }
        };

        SetData();
        setLoading(false);

        return () => {
            setLoading(true);
            setSorted([]);
        };
    }, [productsBycat, filterCombination, sortList, filtered, isNormal, filterSort]);

    const onChange = (e) => {
        e.preventDefault();
        if (e.target.value === '10') {
            setPageLimit(10);
        } else if (e.target.value === '20') {
            setPageLimit(20);
        } else if (e.target.value === '50') {
            setPageLimit(50);
        } else if (e.target.value === '75') {
            setPageLimit(75);
        } else if (e.target.value === '100') {
            setPageLimit(100);
        }
    };

    useEffect(() => {
        setCurrentPage(
            sorted && sorted.slice(offset, offset + parseInt(pageLimit, 10))
        );
    }, [sorted])

    const onPageChanged = (page) => {
        const offset = (page - 1) * pageLimit;
        setOffset(offset);

        setCurrentPage(
            sorted && sorted.slice(offset, offset + parseInt(pageLimit, 10))
        );
    };

    const onChangeSelect = (e) => {
        e.preventDefault();
        localStorage.setItem('sorting', `${e.target.value}`);
        setFilterSort(e.target.value);
    };

    const onPageChangedReset = (e) => {
        e.preventDefault();
        clearFilters();
    };

    function clearFilters() {
        setForManu([]);
        setForDiag([]);
        setForRes([]);
        setForBright([]);
        setForInter([]);
        setForAngle([]);
        setForTemp([]);
        setForTouch([]);
        setForFormat([]);
        setForPower([]);
        setForSize([]);
        setForCont([]);
        setForInput([]);
        setForOutput([]);
        setForCpu([]);
        setForCpuModel([]);
        setForFunction([]);
        setForOpt([]);
        setIsNormal(true);
        setFiltercombination([]);
        setGetFilters({
            serie: title ? title : '',
            category: title2 ? title2 : '',
            id1: 0,
            id2: 0,
            id3: 0,
            forManu: [],
            forDiag: [],
            forRes: [],
            forBright: [],
            forInter: [],
            forAngle: [],
            forTemp: [],
            forTouch: [],
            forFormat: [],
            forPower: [],
            forSize: [],
            forCont: [],
            forInput: [],
            forCpu: [],
            forCpuModel: [],
            forFunction: [],
            forOutput: [],
            forOpt: [],
        });
        setFilterSort('manufacturer');
    }

    useEffect(() => {
        setCurrentPage(
            sorted && sorted.slice(offset, offset + parseInt(pageLimit, 10))
        );

        // Dennis
        /*document
            .querySelectorAll('.pagination-wrap nav ul li')
            .forEach((el) => {
                el.addEventListener('click', () => {
                    window.scroll({
                        top: 0,
                        left: 0,
                        behavior: 'instant',
                    });
                });
            });*/

        return () => {
            document
                .querySelectorAll('.pagination-wrap nav ul li')
                .forEach((el) => {
                    el.removeEventListener('click', el);
                });

                setTimeout(() => {

                  var selectedpanel1 = document.getElementById("panel1bh-header");
                  var selectedpanel2 = document.getElementById("panel2bh-header");
                  var selectedpanel3 = document.getElementById("panel3bh-header");
                  var selectedpanel4 = document.getElementById("panel4bh-header");
                  var selectedpanel5 = document.getElementById("panel5bh-header");
                  var selectedpanel6 = document.getElementById("panel6bh-header");
                  var selectedpanel7 = document.getElementById("panel7bh-header");
                  var selectedpanel8 = document.getElementById("panel8bh-header");
                  var selectedpanel9 = document.getElementById("panel9bh-header");
                  var selectedpanel10 = document.getElementById("panel10bh-header");
                  var selectedpanel11 = document.getElementById("panel11bh-header");
                  var selectedpanel12 = document.getElementById("panel12bh-header");
                  var selectedpanel13 = document.getElementById("panel13bh-header");
                  var selectedpanel14 = document.getElementById("panel14bh-header");
                  var selectedpanel15 = document.getElementById("panel15bh-header");
                  var selectedpanel16 = document.getElementById("panel16bh-header");
                  var selectedpanel17 = document.getElementById("panel17bh-header");
                  var selectedpanel18 = document.getElementById("panel18bh-header");

                  var selectobject = document.getElementById("sortingDropdown");

                  if(selectobject){
                    console.log('see it')

                    if (!selectedpanel3 || (window.getComputedStyle(selectedpanel3).display === "none")){
                      console.log('works')
                    for (var i=0; i<selectobject.length; i++) {
                        if (selectobject.options[i].value == 'resolutionMax'){
                        selectobject.remove(i);
                      }
                    }
                    }
                    if (!selectedpanel14 || (window.getComputedStyle(selectedpanel14).display === "none")){
                    for (var i=0; i<selectobject.length; i++) {
                        if (selectobject.options[i].value == 'input'){
                        selectobject.remove(i);
                      }
                    }
                    }
                    if (!selectedpanel15 || (window.getComputedStyle(selectedpanel15).display === "none")){
                    for (var i=0; i<selectobject.length; i++) {
                        if (selectobject.options[i].value == 'output'){
                        selectobject.remove(i);
                      }
                    }
                    }
                    if (!selectedpanel16 || (window.getComputedStyle(selectedpanel16).display === "none")){
                    for (var i=0; i<selectobject.length; i++) {
                        if (selectobject.options[i].value == 'cpu'){
                        selectobject.remove(i);
                      }
                    }
                    }
                    if (!selectedpanel17 || (window.getComputedStyle(selectedpanel17).display === "none")){
                    for (var i=0; i<selectobject.length; i++) {
                        if (selectobject.options[i].value == 'cpuModel'){
                        selectobject.remove(i);
                      }
                    }
                    }
                    if (!selectedpanel18 || (window.getComputedStyle(selectedpanel18).display === "none")){
                    for (var i=0; i<selectobject.length; i++) {
                        if (selectobject.options[i].value == 'functionality'){
                        selectobject.remove(i);
                      }
                    }
                    }
                if (!selectedpanel4 || (window.getComputedStyle(selectedpanel4).display === "none")){
                for (var i=0; i<selectobject.length; i++) {
                    if (selectobject.options[i].value == 'brightness'){
                    selectobject.remove(i);
                  }
                }
                }

                if (!selectedpanel5 || (window.getComputedStyle(selectedpanel5).display === "none")){
                for (var i=0; i<selectobject.length; i++) {
                    if (selectobject.options[i].value == 'interface'){
                    selectobject.remove(i);
                  }
                }
                }

                if (!selectedpanel6 || (window.getComputedStyle(selectedpanel6).display === "none")){
                for (var i=0; i<selectobject.length; i++) {
                    if (selectobject.options[i].value == 'perspective'){
                    selectobject.remove(i);
                  }
                }
                }

                if (!selectedpanel7 || (window.getComputedStyle(selectedpanel7).display === "none")){
                for (var i=0; i<selectobject.length; i++) {
                    if (selectobject.options[i].value == 'temperatureRange'){
                    selectobject.remove(i);
                  }
                }
                }

                if (!selectedpanel8 || (window.getComputedStyle(selectedpanel8).display === "none")){
                for (var i=0; i<selectobject.length; i++) {
                    if (selectobject.options[i].value == 'touch'){
                    selectobject.remove(i);
                  }
                }
                }

                if (!selectedpanel9 || (window.getComputedStyle(selectedpanel9).display === "none")){
                for (var i=0; i<selectobject.length; i++) {
                    if (selectobject.options[i].value == 'characteristics'){
                    selectobject.remove(i);
                  }
                }
                }

                if (!selectedpanel10 || (window.getComputedStyle(selectedpanel10).display === "none")){
                for (var i=0; i<selectobject.length; i++) {
                    if (selectobject.options[i].value == 'powerSupply'){
                    selectobject.remove(i);
                  }
                }
                }

                if (!selectedpanel1 || (window.getComputedStyle(selectedpanel1).display === "none")){
                for (var i=0; i<selectobject.length; i++) {
                    if (selectobject.options[i].value == 'manufacturer'){
                    selectobject.remove(i);
                  }
                }
                }

                if (!selectedpanel11 || (window.getComputedStyle(selectedpanel11).display === "none")){
                for (var i=0; i<selectobject.length; i++) {
                    if (selectobject.options[i].value == 'diagonale'){
                    selectobject.remove(i);
                  }
                }
                }

                if (!selectedpanel2 || (window.getComputedStyle(selectedpanel2).display === "none")){
                for (var i=0; i<selectobject.length; i++) {
                    if (selectobject.options[i].value == 'sizeDiagonal'){
                    selectobject.remove(i);
                  }
                }
                }

                if (!selectedpanel12 || (window.getComputedStyle(selectedpanel12).display === "none")){
                for (var i=0; i<selectobject.length; i++) {
                    if (selectobject.options[i].value == 'controller'){
                    selectobject.remove(i);
                  }
                }
                }

                if (!selectedpanel13 || (window.getComputedStyle(selectedpanel13).display === "none")){
                for (var i=0; i<selectobject.length; i++) {
                    if (selectobject.options[i].value == 'options'){
                    selectobject.remove(i);
                  }
                }
                }

                }
                }, "1000");



            setCurrentPage([]);
        };

        // eslint-disable-next-line
    }, [offset, sorted, sortList, pageLimit]);

    const ascendingOrder = () => {
        setIsDisabled(true);
        setIsDisabled2(false);
        setSort('asc');
    };

    const descendingOrder = () => {
        setIsDisabled(false);
        setIsDisabled2(true);
        setSort('des');
    };

    useEffect(() => {
        const RunFunction = () => {
            localStorage.setItem('perpage', `${pageLimit}`);
            setCurrentPage(sorted && sorted.slice(offset, offset + pageLimit));
            setPerpage(Number(localStorage.getItem('perpage')));
        };

        RunFunction();

        return () => {
            setCurrentPage([]);
        };

        // eslint-disable-next-line
    }, [pageLimit]);

    useEffect(() => {
        const FinalEffect = () => {
            localStorage.setItem('alphabetical', `${sortList}`);
            if (sortList === 'asc') {
                setIsDisabled(true);
                setIsDisabled2(false);
            } else {
                setIsDisabled(false);
                setIsDisabled2(true);
            }
            setLoading(false);
        };

        FinalEffect();
        return () => {
            setLoading(true);
        };
    }, [sortList]);

    // This is only for
    // the sidebar auto scroll
    // widht the filter options
    useEffect(() => {
        function paralAb() {
            let theOffsetAb = window.pageYOffset;
            let filterBlock = document.getElementById('filters-container');
            let theBoundary = document.getElementById('filter-boundary');
            let filterContainer = document.getElementById(
                'products-list-container'
            );

            let start = '';
            let end = '';
            let box = filterContainer !== null ? filterBlock.offsetHeight : '';

            if (
                filterContainer !== null &&
                filterContainer !== null &&
                theBoundary !== null
            ) {
                start = theBoundary.offsetTop + 150;
                end =
                    filterContainer.offsetTop +
                    filterContainer.offsetHeight -
                    box +
                    filterBlock.offsetHeight -
                    250;
            }

            if (
                theOffsetAb > start &&
                theOffsetAb < end &&
                filterContainer !== null
            ) {
                filterBlock.style.transform =
                    'translateY(' + (theOffsetAb - start) + 'px)';
            } else if (
                filterBlock &&
                filterContainer &&
                filterBlock.offsetHeight > filterContainer.offsetHeight
            ) {
                filterBlock.style.transform = 'none';
                filterBlock.style.bottom = 0;
            }
        }

        document.addEventListener('scroll', paralAb, true);

        return () => {
            document.removeEventListener('scroll', paralAb, true);
        };
    });
    // This is only for
    // the sidebar auto scroll
    // widht the filter options

    useEffect(() => {
        const SetSelect = () => {

          const foundManu = optionsSort.some(el => el.type === 'manufacturer');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].manufacturer.length > 0 &&
                !foundManu &&
                optionsSort.push({
                    type: 'manufacturer',
                    label: 'Manufacturer',
                });

            const foundSize = optionsSort.some(el => el.type === 'sizeDiagonal');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].sizeDiagonal.length > 0 &&
                !foundSize &&
                optionsSort.push({
                    type: 'sizeDiagonal',
                    label: 'Size Diagonal',
                });

            const foundRes = optionsSort.some(el => el.type === 'resolutionMax');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].resolutionMax.length > 0 &&
                !foundRes &&
                optionsSort.push({
                    type: 'resolutionMax',
                    label: 'Resolution (max)',
                });

            const foundBright = optionsSort.some(el => el.type === 'brightness');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].brightness.length > 0 &&
                !foundBright &&
                optionsSort.push({
                    type: 'brightness',
                    label: 'Brightness [cd/m²]',
                });
            const foundInt = optionsSort.some(el => el.type === 'interface');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].interface.length > 0 &&
                !foundInt &&
                optionsSort.push({ type: 'interface', label: 'Interface' });

            const foundPersp = optionsSort.some(el => el.type === 'perspective');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].perspective.length > 0 &&
                !foundPersp &&
                optionsSort.push({
                    type: 'perspective',
                    label: 'Viewing Angle U/D/L/R',
                });


            const foundTemp = optionsSort.some(el => el.type === 'temperatureRange');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].temperatureRange.length > 0 &&
                !foundTemp &&
                optionsSort.push({
                    type: 'temperatureRange',
                    label: 'Temperature Range',
                });

            const foundCpu = optionsSort.some(el => el.type === 'cpu');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].cpu.length > 0 &&
                !foundCpu &&
                optionsSort.push({
                    type: 'cpu',
                    label: 'CPU Type',
                });


            const foundCpuModel = optionsSort.some(el => el.type === 'cpuModel');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].cpuModel.length > 0 &&
                !foundCpuModel &&
                optionsSort.push({
                    type: 'cpuModel',
                    label: 'CPU',
                });

            const foundFunction = optionsSort.some(el => el.type === 'functionality');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].functionality.length > 0 &&
                !foundFunction &&
                optionsSort.push({
                    type: 'functionality',
                    label: 'Function',
                });

            const foundTouch = optionsSort.some(el => el.type === 'touch');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].touch.length > 0 &&
                !foundTouch &&
                optionsSort.push({
                    type: 'touch',
                    label: 'Touch (No/Integrated/Optional)',
                });

            const foundChar = optionsSort.some(el => el.type === 'characteristics');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].characteristics.length > 0 &&
                !foundChar &&
                optionsSort.push({
                    type: 'characteristics',
                    label: 'Format',
                });

            const foundPower = optionsSort.some(el => el.type === 'powerSupply');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].powerSupply.length > 0 &&
                !foundPower &&
                optionsSort.push({
                    type: 'powerSupply',
                    label: 'Power Supply',
                });

            const foundDiag = optionsSort.some(el => el.type === 'diagonale');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].diagonale.length > 0 &&
                !foundDiag &&
                optionsSort.push({
                    type: 'diagonale',
                    label: 'Size Range',
                });

            const foundCont = optionsSort.some(el => el.type === 'controller');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].controller.length > 0 &&
                !foundCont &&
                optionsSort.push({
                    type: 'controller',
                    label: 'Controller',
                });

          const foundInput = optionsSort.some(el => el.type === 'input');

          initialFilters &&
              initialFilters[0] &&
              initialFilters[0].input.length > 0 &&
              !foundInput &&
              optionsSort.push({
                  type: 'input',
                  label: 'Input',
              });

              const foundOutput = optionsSort.some(el => el.type === 'output');

              initialFilters &&
                  initialFilters[0] &&
                  initialFilters[0].output.length > 0 &&
                  !foundOutput &&
                  optionsSort.push({
                      type: 'output',
                      label: 'Output',
                  });

            const foundOption = optionsSort.some(el => el.type === 'options');

            initialFilters &&
                initialFilters[0] &&
                initialFilters[0].options.length > 0 &&
                !foundOption &&
                optionsSort.push({
                    type: 'options',
                    label: 'Options',
                });


        };
        return SetSelect();
        // eslint-disable-next-line

    }, [initialFilters]);


    return loading ? (
        <Spinning />
    ) : productList.error && productsBycat.length === 0 ? (
        <section className='no-product'></section>
    ) : (
        <div id='filter-boundary' className='products-block'>
            <section>
                <Grid container justifyContent='center' spacing={5}>
                    <Grid item xs={12} sm={12} md={4} lg={3}>
                        <div
                            id='products-list-container'
                            className='filter-box-wrapper'
                        >

                                <FiltersSidebar
                                    filterCombination={
                                        filterCombination
                                            ? filterCombination
                                            : []
                                    }
                                    initialFilters={
                                        initialFilters ? initialFilters[0] : []
                                    }
                                    products={productsBycat}
                                    error={error}
                                    forManu={forManu}
                                    setForManu={setForManu}
                                    forDiag={forDiag}
                                    setForDiag={setForDiag}
                                    forRes={forRes}
                                    setForRes={setForRes}
                                    forBright={forBright}
                                    setForBright={setForBright}
                                    forInter={forInter}
                                    setForInter={setForInter}
                                    forAngle={forAngle}
                                    setForAngle={setForAngle}
                                    forTemp={forTemp}
                                    setForTemp={setForTemp}
                                    forTouch={forTouch}
                                    setForTouch={setForTouch}
                                    forFormat={forFormat}
                                    setForFormat={setForFormat}
                                    forPower={forPower}
                                    setForPower={setForPower}
                                    forSize={forSize}
                                    setForSize={setForSize}
                                    forCont={forCont}
                                    setForCont={setForCont}
                                    forOpt={forOpt}
                                    setForOpt={setForOpt}
                                    forInput={forInput}
                                    setForInput={setForInput}
                                    forOutput={forOutput}
                                    setForOutput={setForOutput}
                                    forCpu={forCpu}
                                    setForCpu={setForCpu}
                                    forCpuModel={forCpuModel}
                                    setForCpuModel={setForCpuModel}
                                    forFunction={forFunction}
                                    setForFunction={setForFunction}
                                    setFiltercombination={setFiltercombination}
                                    noFilterResults={noFilterResults}
                                    pathname={pathname}
                                    setIsNormal={setIsNormal}
                                    setGetFilters={setGetFilters}
                                />

                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={8} lg={9}>
                        <Grid container style={{ marginBottom: 70 }}>
                            <Grid
                                item
                                xs={12}
                                className='top-sorting-container'
                            >
                                <div className='top-sorting'>
                                    <span>Items Per Page:</span>
                                    <select
                                        className='sorting-dropdown'
                                        name='perpage'
                                        value={pageLimit}
                                        onChange={(e) => onChange(e)}
                                    >
                                        <option
                                            className='sorting-dropdown-option'
                                            value='10'
                                        >
                                            10
                                        </option>
                                        <option
                                            className='sorting-dropdown-option'
                                            value='20'
                                        >
                                            20
                                        </option>
                                        <option
                                            className='sorting-dropdown-option'
                                            value='50'
                                        >
                                            50
                                        </option>
                                        <option
                                            className='sorting-dropdown-option'
                                            value='75'
                                        >
                                            75
                                        </option>
                                        <option
                                            className='sorting-dropdown-option'
                                            value='100'
                                        >
                                            100
                                        </option>
                                    </select>
                                </div>
                                <div className='filter-dropdown top-sorting'>
                                    <span>Sort By:</span>
                                    {optionsSort.length > 0 && (
                                        <select
                                            id="sortingDropdown"
                                            className='sorting-dropdown'
                                            name='filtersort'
                                            onChange={(e) => onChangeSelect(e)}
                                            defaultValue={filterSort}
                                            style={{
                                                textTransform: 'none',
                                                fontFamily: 'var(--fontuno)',
                                            }}
                                        >
                                            {optionsSort.map((op, index) => (
                                                <option
                                                    key={index}
                                                    className='sorting-dropdown-option'
                                                    value={op.type}
                                                >
                                                    {op.label}
                                                </option>
                                            ))}
                                        </select>
                                    )}
                                </div>
                                <div className='top-sorting'>
                                    <span>Order:</span>
                                    <div className='aphabetical'>
                                        <button
                                            onClick={ascendingOrder}
                                            className='apollo-button filter-buttons'
                                            disabled={isDisabled}
                                        >
                                            Ascending
                                        </button>{' '}
                                        <button
                                            onClick={descendingOrder}
                                            className='apollo-button filter-buttons'
                                            disabled={isDisabled2}
                                        >
                                            Descending
                                        </button>
                                    </div>
                                </div>
                            </Grid>
                        </Grid>

                        {(currentPage.length > 0 && !error && isNormal) ||
                        (currentPage.length > 0 && !error && !isNormal) ||
                        (currentPage.length > 0 && error && isNormal) ? (
                            <div className='list-container'>
                                {currentPage.length > 0 &&
                                    currentPage.map((product) => (
                                        <ProductItem
                                            pathname={pathname}
                                            product={product}
                                            key={product._id}
                                        />
                                    ))}
                            </div>
                        ) : (
                            error &&
                            !isNormal && (
                                <div style={{ textAlign: 'center' }}>
                                    <h4>
                                        There is no products with this filter
                                        combination
                                    </h4>
                                    <div className='button-read-more'>
                                        <button
                                            onClick={(e) =>
                                                onPageChangedReset(e)
                                            }
                                            className='apollo-button'
                                            style={{
                                                boder: 'none',
                                                marginLeft: 20,
                                            }}
                                        >
                                            Clear Filters
                                        </button>
                                    </div>
                                </div>
                            )
                        )}

                        <Grid container justifyContent='center'>
                            <Grid item xs={12} sm={5} md={4}>
                                <div className='pagination-wrap'>
                                    {sorted && (
                                        <Pagination
                                            totalRecords={sorted.length}
                                            pageLimit={perpage}
                                            pageRangeDisplayed={3}
                                            onChangePage={onPageChanged}
                                        />
                                    )}
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </section>
        </div>
    );
};

// const delDuplicates = (array) => [...new Set(array)];

export default ProductsList;
